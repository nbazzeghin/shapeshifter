/*
   SHAPESHIFTER DIY OPEN SOURCE DRUM MACHINE
   by FASELUNARE.COM

   https://github.com/faselunare/shapeshifter
   Released under the GPLv3 license.

*/

#include "ShapeShifter.h"
#include <Audio.h>

const size_t jsonCapacity = 4*JSON_ARRAY_SIZE(8) + JSON_OBJECT_SIZE(4) + 440;

AudioPlaySdWav           SDtracks[NUMBER_OF_TRACKS] = {AudioPlaySdWav(), AudioPlaySdWav(), AudioPlaySdWav(), AudioPlaySdWav()};
AudioMixer4              mixABTrack;      //xy=322,186
AudioMixer4              mixCDTrack;      //xy=325,286
AudioSynthWaveformDc     dc1;            //xy=520,309
AudioMixer4              source_mixer;   //xy=546,240
AudioEffectMultiply      multiply1;      //xy=706,291
AudioFilterBiquad        filter1;        //xy=870,299
AudioEffectBitcrusher    bitcrusher1;    //xy=1060,297
AudioOutputAnalog        dac1;           //xy=1234,297
AudioConnection          patchCord1(SDtracks[0], 0, mixABTrack, 0);
AudioConnection          patchCord2(SDtracks[0], 1, mixABTrack, 1);
AudioConnection          patchCord3(SDtracks[1], 0, mixABTrack, 2);
AudioConnection          patchCord4(SDtracks[1], 1, mixABTrack, 3);
AudioConnection          patchCord5(SDtracks[2], 0, mixCDTrack, 0);
AudioConnection          patchCord6(SDtracks[2], 1, mixCDTrack, 1);
AudioConnection          patchCord7(SDtracks[3], 0, mixCDTrack, 2);
AudioConnection          patchCord8(SDtracks[3], 1, mixCDTrack, 3);
AudioConnection          patchCord9(mixABTrack, 0, source_mixer, 0);
AudioConnection          patchCord10(mixCDTrack, 0, source_mixer, 1);
AudioConnection          patchCord11(dc1, 0, multiply1, 1);
AudioConnection          patchCord12(source_mixer, 0, multiply1, 0);
AudioConnection          patchCord13(multiply1, filter1);
AudioConnection          patchCord14(filter1, bitcrusher1);
AudioConnection          patchCord15(bitcrusher1, dac1);

Bounce controlButtonsBounce[NUMBER_OF_TRACKS] = {Bounce(), Bounce(), Bounce(), Bounce()};

void ShapeShifter::begin() {
#ifdef DEBUG
  Serial.begin(115200);
  //while(!Serial);
  Serial.println("Shape Shifter!");
#endif
  currentDrum = 0;
  selectSound = 0;

  SPI.setMOSI(SDCARD_MOSI_PIN);
  SPI.setSCK(SDCARD_SCK_PIN);

  pinMode(SAVE_BUTTON, INPUT_PULLUP);
  pinMode(LOAD_BUTTON, INPUT_PULLUP);

  for (int i = 0; i < NUMBER_OF_SUBPATTERNS; i++) {
    mixerGain[i] = 0.7;
    subpatterns[i].currentSubpatternLength = NUMBER_OF_STEPS;
    subpatterns[i].currentStep = 1;
  }

  for (int i = 0; i < NUMBER_OF_STEPS; i ++) {
    pinMode(patternButtons[i], INPUT_PULLUP);
  }

  for (int i = 0; i < NUMBER_OF_CONTROLS; i ++) {
    pinMode(controlLEDs[i], OUTPUT);
    pinMode(controlButtons[i], INPUT_PULLUP);
    if(i < NUMBER_OF_TRACKS) {
      subpatterns[i].chosenSample = 1;
      controlButtonsBounce[i].attach(tracksButtons[i]);
      controlButtonsBounce[i].interval(BUTTON_DEBOUNCE);
    }
  }

  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  for (int i = 0; i < 2; i ++) {
    writeDual595(255, 255);
    writeDual595(0, 0);
  }
  
  initAudio();

  do {
      driveAllLEDs(false);
      delay(500);
      driveAllLEDs(true);
      delay(500);
  } while (!SD.begin(BUILTIN_SDCARD));

  driveAllLEDs(false);
#ifdef DEBUG
  Serial.println("Inizialization done");
#endif
}

void ShapeShifter::run() {
#ifdef DEBUG
  debug();
#endif

  if(!digitalRead(controlButtons[PLAY_POS])) {
      previousPlay = !previousPlay;
#ifdef DEBUG
      Serial.println("PLAY PRESSED");
#endif
      delay(200);
  }

  if(previousPlay) {
    updateTiming();
  }
  else {
    updateTime = millis();
  }

  if(!digitalRead(SAVE_BUTTON) || !digitalRead(LOAD_BUTTON)) {
    if(!operationDone) { //avoid multiple saving
      int chosenSlot = updatePatternButtons(true);
      if(chosenSlot != NO_BUTTON) {
        for(int i = 0; i < NUMBER_OF_TRACKS; i++) {
          SDtracks[i].stop();
        }
        if(!digitalRead(SAVE_BUTTON)) {
          saveJson(chosenSlot);
        }
        else {
          loadJson(chosenSlot);
        }
      }
    }
  }
  else {
    updatePatternButtons(false);
    operationDone = false;
  }
  updateControlButtons();
}

void ShapeShifter:: driveAllLEDs(bool OnOffn) {
  for(uint8_t i = 0; i < sizeof(controlLEDs)/sizeof(uint8_t); i++) {
    digitalWrite(controlLEDs[i], OnOffn);
  }
  if (OnOffn) {
    setLEDs(16);
  }
  else {
    setLEDs(0);
  }
}

void ShapeShifter::initAudio() {
  AudioMemory(250);
  AudioNoInterrupts();

  filter1.setLowpass(0, 800, 0.54);
  filter1.setLowpass(1, 800, 1.3);
  filter1.setLowpass(2, 800, 0.54);
  filter1.setLowpass(3, 800, 1.3);

  // BitCrusher
  int currentSampleRate = 22050; // this defaults to passthrough.
  bitcrusher1.bits(NUMBER_OF_STEPS); //set the crusher to defaults. This will passthrough clean at 16,44100
  bitcrusher1.sampleRate(currentSampleRate); //

  for (int i = 0; i < NUMBER_OF_TRACKS; i++) {
    source_mixer.gain(i, mixerGain[i]);
    source_mixer.gain(i, mixerGain[i]);
    source_mixer.gain(i, mixerGain[i]);
    source_mixer.gain(i, mixerGain[i]);

    mixABTrack.gain(i, mixerGain[i]);
    mixABTrack.gain(i, mixerGain[i]);
    mixABTrack.gain(i, mixerGain[i]);
    mixABTrack.gain(i, mixerGain[i]);

    mixCDTrack.gain(i, mixerGain[i]);
    mixCDTrack.gain(i, mixerGain[i]);
    mixCDTrack.gain(i, mixerGain[i]);
    mixCDTrack.gain(i, mixerGain[i]);
  }

  dc1.amplitude(0.7);
  AudioInterrupts();
}

void ShapeShifter::updateTiming() {
  if((millis() - updateTime) > (unsigned long) (tempo)) {
    updateTime = millis();
    updateStep(); 
  }
  else {
    setLEDs(subpatterns[currentDrum].currentStep - 1, true);
  }
}

void ShapeShifter::updateStep() {
#ifdef DEBUG
  Serial.print("Drum: ");
  Serial.print(currentDrum);
  Serial.print(" Step: ");
  Serial.println(subpatterns[currentDrum].currentStep);
#endif
  playSample();

  if (subpatterns[currentDrum].currentStep % 4 == 0) {
    digitalWriteFast(controlLEDs[PLAY_POS], HIGH);
  } else {
    digitalWriteFast(controlLEDs[PLAY_POS], LOW);
  }

  //update the steps for each track
  for(int i = 0; i < NUMBER_OF_TRACKS; i++) {
    if(subpatterns[i].currentStep < subpatterns[i].currentSubpatternLength) {
      subpatterns[i].currentStep++;
    }
    else {
      subpatterns[i].currentStep = 1;
    }
  }

  tempo = 60*(1 + map(analogRead(tempoPot), 0, 1023, 0, 7));
  lowPass = analogRead(lowPassPot);
  if (lowPass != lowPassPrevious) {
    lowPassPrevious = lowPass;
    filter1.setLowpass(0, (lowPass * 20) + 10, 0.54);
    filter1.setLowpass(1, (lowPass * 20) + 10, 1.3);
    filter1.setLowpass(2, (lowPass * 20) + 10, 0.54);
    filter1.setLowpass(3, (lowPass * 20) + 10, 1.3);
  }

  bitCrush = analogRead(bitCrushPot);
  if (bitCrush != bitCrushPrevious) {
    bitCrushPrevious = bitCrush;
    bitcrusher1.bits(map(bitCrush, 0, 1023, 0, NUMBER_OF_STEPS));
  }
}

void ShapeShifter::playSample() {
  for (int i = 0; i < NUMBER_OF_TRACKS; i++) {
    if (bitRead(pattern[i][subpatterns[i].currentSubpattern], subpatterns[i].currentStep - 1)) {
      selectTracksSamples(i);
      digitalWrite(tracksLEDs[i], HIGH);
    }
  }
}

void ShapeShifter::updateControlButtons() {
  mode = 1 - digitalReadFast(controlButtons[FN_POS]);
  if (mode != previousMode) {
    previousMode = mode;
    digitalWrite(controlLEDs[FN_POS], mode);
  }
  if (mode == 0) {
    clearTracksLEDs();
    digitalWrite(tracksLEDs[currentDrum], HIGH);
    updateLEDsPattern();
  } else {
    clearTracksLEDs();
    digitalWrite(tracksLEDs[subpatterns[currentDrum].currentSubpattern], HIGH);
    setLEDs(subpatterns[currentDrum].currentSubpatternLength);
  }

  for (int i = 0; i < NUMBER_OF_TRACKS; i++) {
    controlButtonsBounce[i].update();
    if(controlButtonsBounce[i].fell()) {
#ifdef DEBUG
      Serial.print("Control button: ");
      switch(tracksButtons[i]) {
        case 30: Serial.println("Track A"); break;
        case 28: Serial.println("Track B"); break;
        case 26: Serial.println("Track C"); break;
        case 24: Serial.println("Track D"); break;
      }
#endif
      if (mode) {
        clearTracksLEDs();
        digitalWrite(tracksLEDs[i], HIGH);
        subpatterns[currentDrum].currentSubpattern = i;
      }
      else {
        if (currentDrum != i) {
          clearTracksLEDs();
          digitalWrite(tracksLEDs[i], HIGH);
          currentDrum = i;
        }
        else {
          setLEDs(subpatterns[i].chosenSample, true);
          selectSound = i + 1;
          #ifdef DEBUG
        Serial.println("ROSE");
#endif
        }
      }
    }
    if (controlButtonsBounce[i].rose() && (selectSound == i + 1) && (currentDrum == i)) {
      selectSound = 0;
      updateLEDsPattern();
    }
  }
}

// Read push buttons
int ShapeShifter::updatePatternButtons(bool loadOrSave) {
  int lastPressedButton = -1;
  unsigned int currentpatternButtons = 0;
  if(loadOrSave) {
    driveAllLEDs(false); //clear all the LEDs
    for (uint8_t i = 0; i < NUMBER_OF_STEPS; i ++) {
      if(!digitalRead(patternButtons[i])) {
        lastPressedButton = i;
      }
    }
    if(lastPressedButton != -1) {
#ifdef DEBUG
      Serial.print("\nChose: ");
      Serial.println(lastPressedButton);
#endif
    }
  }

  else {
    if(millis() - startTime > DEBOUNCING_TIME_STEPS) {
      for (uint8_t i = 0; i < NUMBER_OF_STEPS; i ++) {
        bitWrite(currentpatternButtons, i, digitalRead(patternButtons[i]));
      }
      if (currentpatternButtons != previouspatternButtons) {
        startTime = millis();
        unsigned int fallingEdgepatternButtons = findDifferences(currentpatternButtons, previouspatternButtons, FALLING_EDGE);
        uint8_t highValue = 0;
        previouspatternButtons = currentpatternButtons;
        if(fallingEdgepatternButtons > 0) {
          if (mode == 1) {
            for (int i = 0; i < NUMBER_OF_STEPS; i ++) {
              if (bitRead(fallingEdgepatternButtons, i)) {
                highValue = i;
              }
            }
            subpatterns[currentDrum].currentSubpatternLength = highValue + 1;
            setLEDs(highValue + 1);
          }
          else {
            if (selectSound == 0) {
              for (uint8_t i = 0; i < NUMBER_OF_STEPS; i ++) {
                if (bitRead(fallingEdgepatternButtons, i)) {
                  bitWrite(pattern[currentDrum][subpatterns[currentDrum].currentSubpattern], i, !bitRead(pattern[currentDrum][subpatterns[currentDrum].currentSubpattern], i)); //flip the bit
                  updateLEDsPattern();
                }
              }
            }
            else {
              for (int i = 0; i < NUMBER_OF_STEPS; i ++) {
                if (bitRead(fallingEdgepatternButtons, i)) {
                  highValue = i + 1;
                }
              }
              subpatterns[selectSound - 1].chosenSample = highValue;
              setLEDs(highValue, true);
            }
          }
        }
      }
    }
  }

  return lastPressedButton;
}

unsigned int ShapeShifter::findDifferences(unsigned int current, unsigned int previous, bool risingFallingn) {
  unsigned int edges = 0;
  for (int i = 0; i < NUMBER_OF_STEPS; i ++) {
    if (bitRead(current, i) != bitRead(previous, i)){
      if(bitRead(current, i) == risingFallingn) {
        bitSet(edges, i);
#ifdef DEBUG
        Serial.print("Bit ");
        Serial.println(i);
#endif
      }
    }
  }
  return edges;
}

// Update LEDs with current pattern array area
void ShapeShifter::updateLEDsPattern() {
  writeDual595(lowByte(pattern[currentDrum][subpatterns[currentDrum].currentSubpattern]), highByte(pattern[currentDrum][subpatterns[currentDrum].currentSubpattern]));
}

// Set LEDs to particular linear value (0 - 16)
void ShapeShifter::setLEDs(uint8_t value, bool single) {
  uint16_t work = 0;
  if (value == 0) {
    writeDual595(0, 0);
  }
  else {
    if(single) {
      work = 1 << (value - 1);
    }
    else {
      for (int i = 0; i < value; i ++) {
        bitSet(work, i);
      }
    }
    writeDual595(lowByte(work), highByte(work));
  }
}

// Write two uint8_ts to both 595s
void ShapeShifter::writeDual595(uint8_t data1, uint8_t data2) {
  digitalWriteFast(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, data2);
  shiftOut(dataPin, clockPin, MSBFIRST, data1);
  digitalWriteFast(latchPin, HIGH);
}

void ShapeShifter::clearTracksLEDs() {
  for (int i = 0; i < NUMBER_OF_TRACKS; i ++) {
    digitalWrite(tracksLEDs[i], LOW);
  }
}

void ShapeShifter::selectTracksSamples(int trackNumber) {
  String filename = "tr" + String(trackNumber + 1) + "_" + String(subpatterns[trackNumber].chosenSample) + ".wav";
#ifdef DEBUG
  Serial.print("PLAY SAMPLE" + filename + "\n");
#endif
  SDtracks[trackNumber].play(filename.c_str());
}

bool ShapeShifter::saveJson(int thisFileNumber) {
  
  DynamicJsonBuffer jsonBuffer(jsonCapacity);
  JsonObject& root = jsonBuffer.createObject();

  JsonArray& trackA = root.createNestedArray("trackA");
  trackA.add(getBinary16String(pattern[0][0]));
  trackA.add(getBinary16String(pattern[0][1]));
  trackA.add(getBinary16String(pattern[0][2]));
  trackA.add(getBinary16String(pattern[0][3]));
  trackA.add(subpatterns[0].currentSubpattern);
  trackA.add(subpatterns[0].currentSubpatternLength);
  trackA.add(subpatterns[0].chosenSample);

  JsonArray& trackB = root.createNestedArray("trackB");
  trackB.add(getBinary16String(pattern[1][0]));
  trackB.add(getBinary16String(pattern[1][1]));
  trackB.add(getBinary16String(pattern[1][2]));
  trackB.add(getBinary16String(pattern[1][3]));
  trackB.add(subpatterns[1].currentSubpattern);
  trackB.add(subpatterns[1].currentSubpatternLength);
  trackB.add(subpatterns[1].chosenSample);

  JsonArray& trackC = root.createNestedArray("trackC");
  trackC.add(getBinary16String(pattern[2][0]));
  trackC.add(getBinary16String(pattern[2][1]));
  trackC.add(getBinary16String(pattern[2][2]));
  trackC.add(getBinary16String(pattern[2][3]));
  trackC.add(subpatterns[2].currentSubpattern);
  trackC.add(subpatterns[2].currentSubpatternLength);
  trackC.add(subpatterns[2].chosenSample);

  JsonArray& trackD = root.createNestedArray("trackD");
  trackD.add(getBinary16String(pattern[3][0]));
  trackD.add(getBinary16String(pattern[3][1]));
  trackD.add(getBinary16String(pattern[3][2]));
  trackD.add(getBinary16String(pattern[3][3]));
  trackD.add(subpatterns[3].currentSubpattern);
  trackD.add(subpatterns[3].currentSubpatternLength);
  trackD.add(subpatterns[3].chosenSample);

  String filename = "work" + String(thisFileNumber) + ".jsn";
  SD.remove(filename.c_str());
  File dataFile = SD.open(filename.c_str(), FILE_WRITE);
  if (dataFile) {
    root.prettyPrintTo(dataFile);
    dataFile.close();
#ifdef DEBUG
    root.prettyPrintTo(Serial);
#endif
    operationDone = true;
    return true;
  }  
  else {
#ifdef DEBUG
    Serial.print("error opening ");
    Serial.println(filename.c_str());
#endif
    return false;
  }
}

bool ShapeShifter::loadJson(int thisFileNumber) {
  String filename = "work" + String(thisFileNumber) + ".jsn";
  File dataFile = SD.open(filename.c_str());

  StaticJsonBuffer<jsonCapacity> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(dataFile);

  if (root.success()) {
#ifdef DEBUG
    Serial.println("LOADED");
    root.prettyPrintTo(Serial);
#endif
    const char* thisDataString;

    JsonArray& trackA = root["trackA"];
    thisDataString = trackA[0];
    pattern[0][0] = getPatternFromString(String(thisDataString));
    thisDataString = trackA[1];
    pattern[0][1] = getPatternFromString(String(thisDataString));
    thisDataString = trackA[2];
    pattern[0][2] = getPatternFromString(String(thisDataString));
    thisDataString = trackA[3];
    pattern[0][3] = getPatternFromString(String(thisDataString));
    subpatterns[0].currentSubpattern = trackA[4];
    subpatterns[0].currentSubpatternLength = trackA[5];
    subpatterns[0].chosenSample = trackA[6];
    subpatterns[0].currentStep = 1;

    JsonArray& trackB = root["trackB"];
    thisDataString = trackB[0];
    pattern[1][0] = getPatternFromString(String(thisDataString));
    thisDataString = trackB[1];
    pattern[1][1] = getPatternFromString(String(thisDataString));
    thisDataString = trackB[2];
    pattern[1][2] = getPatternFromString(String(thisDataString));
    thisDataString = trackB[3];
    pattern[1][3] = getPatternFromString(String(thisDataString));
    subpatterns[1].currentSubpattern = trackB[4];
    subpatterns[1].currentSubpatternLength = trackB[5];
    subpatterns[1].chosenSample = trackB[6];
    subpatterns[1].currentStep = 1;

    JsonArray& trackC = root["trackC"];
    thisDataString = trackC[0];
    pattern[2][0] = getPatternFromString(String(thisDataString));
    thisDataString = trackC[1];
    pattern[2][1] = getPatternFromString(String(thisDataString));
    thisDataString = trackC[2];
    pattern[2][3] = getPatternFromString(String(thisDataString));
    thisDataString = trackC[3];
    pattern[2][4] = getPatternFromString(String(thisDataString));
    subpatterns[2].currentSubpattern = trackC[4];
    subpatterns[2].currentSubpatternLength = trackC[5];
    subpatterns[2].chosenSample = trackC[6];
    subpatterns[2].currentStep = 1;

    JsonArray& trackD = root["trackD"];
    thisDataString = trackD[0];
    pattern[3][0] = getPatternFromString(String(thisDataString));
    thisDataString = trackD[1];
    pattern[3][1] = getPatternFromString(String(thisDataString));
    thisDataString = trackD[2];
    pattern[3][2] = getPatternFromString(String(thisDataString));
    thisDataString = trackD[3];
    pattern[3][3] = getPatternFromString(String(thisDataString));
    subpatterns[3].currentSubpattern = trackD[4];
    subpatterns[3].currentSubpatternLength = trackD[5];
    subpatterns[3].chosenSample = trackD[6];
    subpatterns[3].currentStep = 1;

    digitalWrite(controlLEDs[PLAY_POS], LOW);
    operationDone = true;
    return true;
  }
  else {
#ifdef DEBUG
    Serial.print("error opening ");
    Serial.println(filename.c_str());
#endif
    return false;
  }
}

String ShapeShifter::getBinary16String(unsigned int thisValue) {
  String returnString = "";
  for (int i = 0; i < 16; i++) {
    if (thisValue & (1 << i))
      returnString += "1";
    else
      returnString += "0";
    if (((i + 1) % 4 == 0) && ((i + 1) != 16))
      returnString += " ";
  }

  return returnString;
}

unsigned int ShapeShifter::getPatternFromString(String thisString) {
  unsigned int returnPattern = 0;
  thisString = thisString.replace(" ", "");
  for (int i = 0; i < 16; i++) {
    if (thisString.c_str()[i] == '1')
      returnPattern += (1 << i);
  }

  return returnPattern;
}

#ifdef DEBUG
void ShapeShifter::debug() {
  String command = parseCommand();
  command.trim();

  if(command != "") {
    Serial.println(command);
  }

  if (command == "pattern?") {
    for (int i = 0; i < NUMBER_OF_TRACKS; i++) {
      for (int j = 0; j < NUMBER_OF_SUBPATTERNS; j++) {
        Serial.print("Pattern[");
        Serial.print(i);
        Serial.print("]");
        Serial.print("[");
        Serial.print(j);
        Serial.print("]: ");
        String thisPatternString = getBinary16String(pattern[i][j]);
#ifdef DEBUG
        Serial.println(thisPatternString);
#endif
      }
      Serial.println();
    }
  }
  else if(command == "saveJson?") {
    saveJson(1);
  }

  else if(command == "loadJson?") {
    loadJson(1);
  }
}

String ShapeShifter::parseCommand() {
  while (Serial.available() > 0)
    return Serial.readString();

  return "";
}

#endif